FROM node:14-alpine
MAINTAINER Florian Kroeber @ https://www.planBLICK.com

RUN apk update && apk add --no-cache shadow git python3

WORKDIR /usr/src/app

RUN npm install -g npm@8.1.0 sass node-sass gulp-cli postcss-cli autoprefixer watch --unsafe-perm
RUN npm rebuild node-sass

COPY www /usr/src/app
RUN npm install

COPY ./runApp.sh /usr/src/runApp.sh
RUN chmod 777 /usr/src/runApp.sh && chmod +x /usr/src/runApp.sh

RUN gulp npm-lib
CMD ["/usr/src/runApp.sh"]